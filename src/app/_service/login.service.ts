import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  isAuthenticate: boolean = false;

  constructor(private http: HttpClient) { }

  authenticated(credentials) {
    console.log(credentials);

    this.isAuthenticate = credentials['username'] === 'will';
    return this.isAuthenticate;
  }

  isAuthenticated() {
    return this.isAuthenticate;
  }
}
