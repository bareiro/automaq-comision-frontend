import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { retry, catchError } from 'rxjs/operators';
import { Factura } from '../_model/factura';

const BASE_URL = 'http://localhost:8080';

@Injectable({
  providedIn: 'root'
})
export class FacturasService {

  constructor(private http: HttpClient) { }

  buscarFacturasPorCodigoVendedor(codigo: string) {
    return this.http.get<Factura[]>(`${BASE_URL}/facturas/seller/${codigo}`);
  }

  buscarFacturasPorRangoFecha(fechaIni: string, fechaFin: string) {
    return this.http.get<Factura[]>(`${BASE_URL}/facturas/sellerRango/${fechaIni}/${fechaFin}`);
  }

}
