import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WelcomeComponent } from './_components/welcome/welcome.component';
import { FacturasVendedorComponent } from './_components/facturas/facturas-vendedor/facturas-vendedor.component';
import { FacturasVendedorMesComponent } from './_components/facturas/facturas-vendedor-mes/facturas-vendedor-mes.component';
import { FacturasVendedorRangofechaComponent } from './_components/facturas/facturas-vendedor-rangofecha/facturas-vendedor-rangofecha.component';
import { ComisionAprobacionComponent } from './_components/gerencia/comision-aprobacion/comision-aprobacion.component';


const routes: Routes = [
  { path: 'home', component: WelcomeComponent },
  {
    path: 'facturas', children: [
      { path: 'vendedor', component: FacturasVendedorComponent },
      { path: 'vendedor/mes', component: FacturasVendedorMesComponent },
      { path: 'vendedor/rangofecha', component: FacturasVendedorRangofechaComponent }
    ]
  },
  {
    path: 'gerencia', children: [
      { path: 'aprobacion/division', component: ComisionAprobacionComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
