import { Component, OnInit } from '@angular/core';
import { Vendedor } from 'src/app/_model/Vendedor';
import { FormGroup, FormControl } from '@angular/forms';
import { FacturasService } from 'src/app/_service/facturas.service';
import { Factura } from 'src/app/_model/factura';

@Component({
  selector: 'app-comision-aprobacion',
  templateUrl: './comision-aprobacion.component.html',
  styleUrls: ['./comision-aprobacion.component.css']
})
export class ComisionAprobacionComponent implements OnInit {

  //// listado
  vendedorList: Vendedor[];
  vendedorFilter: Vendedor[];
  vendedorFacturaList: Factura[];
  //// form group
  filtrarForm: FormGroup;
  //// variables
  vendedorSeleccionado: Vendedor;

  constructor(private facturaService: FacturasService) { }

  ngOnInit() {
      this.vendedorList = this.obtenerVendedorPorDivision('automotores');
      this.vendedorFilter = this.vendedorList;
      this.filtrarForm = new FormGroup({
          filtro: new FormControl('')
      });
  }

  obtenerVendedorFiltrado(valor: any) {
      this.vendedorFilter = this.vendedorList;
      //// filtra por la palabra recogida
      const filtro = valor.target.value.toLowerCase();
      if (filtro.length >= 3) {
        this.vendedorFilter = this.vendedorList.filter(vendedor =>
            vendedor.nombre.toLowerCase().indexOf(filtro) !== -1 ||
            vendedor.sucursal.toLowerCase().indexOf(filtro) !== -1);
      }
  }

  obtenerInfoVendedor(vendedor: Vendedor) {
      this.vendedorSeleccionado = vendedor;
      this.facturaService.buscarFacturasPorCodigoVendedor(vendedor.id).subscribe(
        response => this.vendedorFacturaList = response,
        error => this.handleError(error)
      );
  }


  handleError(error: any): void {
    console.log(error.message);
  }


  obtenerVendedorPorDivision(division: string) {
      return [
        { id: '1', nombre: 'Marco Dario Bareiro Mendoza', sucursal: 'sucursal 1' },
        { id: '2', nombre: 'Wilfredo Rosales', sucursal: 'Casa central' },
        { id: '3', nombre: 'Juan Perez', sucursal: 'Multimarca' },
        { id: '4', nombre: 'Ludovico Peluche', sucursal: 'sucursal 4' },
        { id: '5', nombre: 'Juan Manuel Lopez', sucursal: 'Casa central' },
        { id: '6', nombre: 'Ernesto Guevara', sucursal: 'Multimarca' },
        { id: '7', nombre: 'Nill Born', sucursal: 'sucursal 4' },
        { id: '8', nombre: 'Paul Gilbert', sucursal: 'Casa central' },
        { id: '9', nombre: 'Yngwie Malmsteen', sucursal: 'Multimarca' },
        { id: '10', nombre: 'Berta Rojas', sucursal: 'sucursal 4' },
        { id: '11', nombre: 'Chespirito', sucursal: 'sucursal 3' }
      ];
  }


}
