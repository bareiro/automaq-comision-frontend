import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FacturasService } from 'src/app/_service/facturas.service';

@Component({
  selector: 'app-facturas-vendedor-rangofecha',
  templateUrl: './facturas-vendedor-rangofecha.component.html',
  styleUrls: ['./facturas-vendedor-rangofecha.component.css']
})

export class FacturasVendedorRangofechaComponent implements OnInit {

  buscarForm: FormGroup;

  constructor(private facturaService: FacturasService) { }

  ngOnInit() {
    this.buscarForm = new FormGroup({
      fechaIni: new FormControl('', [Validators.required]),
      fechaFin: new FormControl('', [Validators.required])
    });
  }

  buscarFacturasVendedorRangoFecha() {
    this.facturaService.buscarFacturasPorRangoFecha(this.buscarForm.value.fechaIni, this.buscarForm.value.fechaFin).subscribe(
      response => console.log(response),
      error => this.handleError(error)
    );
  }

  handleError(error: any): void {
    console.log(error.message);
  }

}
