import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FacturasService } from 'src/app/_service/facturas.service';

@Component({
  selector: 'app-facturas-vendedor',
  templateUrl: './facturas-vendedor.component.html',
  styleUrls: ['./facturas-vendedor.component.css']
})
export class FacturasVendedorComponent implements OnInit {

  buscarForm: FormGroup;

  constructor(private facturaService: FacturasService) { }

  ngOnInit() {
    this.buscarForm = new FormGroup({
      codigo: new FormControl('', [
        Validators.required
      ])
    });
  }

  get f() { return this.buscarForm.controls; }

  buscarFacturasVendedorId() {
    this.facturaService.buscarFacturasPorCodigoVendedor(this.buscarForm.value.codigo).subscribe(
      response => console.log(response),
      error => this.handleError(error)
    );

  }
  handleError(error: any): void {
    console.log(error.message);
  }

}
