import { Component, OnInit } from '@angular/core';
import { FacturasService } from 'src/app/_service/facturas.service';
import { Factura } from 'src/app/_model/factura';

@Component({
  selector: 'app-facturas-list',
  templateUrl: './facturas-list.component.html',
  styleUrls: ['./facturas-list.component.css']
})
export class FacturasListComponent implements OnInit {

  facturas: Factura[];

  constructor(private facturasService: FacturasService) { }

  ngOnInit() {
    this.facturas = [];
  }

  handleError(error: any): void {
    alert(error.message);
  }
}
