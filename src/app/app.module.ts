import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WelcomeComponent } from './_components/welcome/welcome.component';
import { HeaderComponent } from './_components/header/header.component';
import { FooterComponent } from './_components/footer/footer.component';
import { FacturasListComponent } from './_components/facturas/facturas-list/facturas-list.component';
import { FacturasFormComponent } from './_components/facturas/facturas-form/facturas-form.component';
import { HttpInterceptor } from './_helpers/http.interceptor';
import { FacturasVendedorComponent } from './_components/facturas/facturas-vendedor/facturas-vendedor.component';
import { FacturasVendedorMesComponent } from './_components/facturas/facturas-vendedor-mes/facturas-vendedor-mes.component';
import { FacturasVendedorRangofechaComponent } from './_components/facturas/facturas-vendedor-rangofecha/facturas-vendedor-rangofecha.component';
import { ComisionAprobacionComponent } from './_components/gerencia/comision-aprobacion/comision-aprobacion.component';


@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    HeaderComponent,
    FooterComponent,
    FacturasListComponent,
    FacturasFormComponent,
    FacturasVendedorComponent,
    FacturasVendedorMesComponent,
    FacturasVendedorRangofechaComponent,
    ComisionAprobacionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: HttpInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
