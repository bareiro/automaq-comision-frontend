import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpHeaders } from '@angular/common/http';

@Injectable()
export class HttpInterceptor implements HttpInterceptor {

    intercept(req: HttpRequest<any>, next: HttpHandler) {
        const headersOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            })
        };
        const request = req.clone({
            headers: req.headers.set("Content-Type", "application/json")
        });
        return next.handle(request);
    }
}
